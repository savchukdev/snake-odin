package main

import "core:fmt"
import "core:intrinsics"
import "core:slice"
import rl "vendor:raylib"

HEIGHT :: 512 // px
WIDTH :: HEIGHT // px

CELL_SIZE :: 32 // px

CELLS_PER_ROW :: WIDTH / CELL_SIZE
TOTAL_CELLS :: CELLS_PER_ROW * CELLS_PER_ROW

STATUS_BAR_HEIGHT :: 44

Vec2 :: [2]int

Grid := [TOTAL_CELLS]Cell{}

Cell :: enum {
  None,
  Snake,
}

snake_wav :: #load("src_snake.wav")
speaker_image_raw :: #load("speaker.png")
game_over_mp3 :: #load("game_over.mp3")

main :: proc() {
  rl.InitWindow(WIDTH, HEIGHT + STATUS_BAR_HEIGHT, "Snake game")
  defer rl.CloseWindow()

  rl.InitAudioDevice()
  if !rl.IsAudioDeviceReady() {
    return
  }
  defer rl.CloseAudioDevice()

  head_velocity: Vec2
  next_velocity: Vec2 // can i get rid of this variable?

  snake := [TOTAL_CELLS]Vec2{}
  snake_length := 1

  snake[0].x = 8
  snake[0].y = 8

  generate_target := proc(head: Vec2) -> Vec2 {
    next_cell := int(rl.GetRandomValue(0, TOTAL_CELLS - 1))

    target := Vec2{next_cell % CELLS_PER_ROW, next_cell / CELLS_PER_ROW}

    for Grid[next_cell] == .Snake {
      next_cell = int(rl.GetRandomValue(0, TOTAL_CELLS - 1))
      target = {next_cell % CELLS_PER_ROW, next_cell / CELLS_PER_ROW}
    }

    return target
  }

  target := generate_target(snake[0])

  music := rl.LoadMusicStreamFromMemory(".wav", rawptr(raw_data(snake_wav)), i32(len(snake_wav)))
  defer rl.UnloadMusicStream(music)
  rl.PlayMusicStream(music)

  game_over_wave := rl.LoadWaveFromMemory(
    ".mp3",
    rawptr(raw_data(game_over_mp3)),
    i32(len(game_over_mp3)),
  )
  game_over_sound := rl.LoadSoundFromWave(game_over_wave)
  rl.UnloadWave(game_over_wave)
  defer rl.UnloadSound(game_over_sound)

  speaker_image := rl.LoadImageFromMemory(
    ".png",
    rawptr(raw_data(speaker_image_raw)),
    i32(len(speaker_image_raw)),
  )
  speaker_texture := rl.LoadTextureFromImage(speaker_image)
  defer rl.UnloadTexture(speaker_texture)
  rl.UnloadImage(speaker_image)

  music_volume := rl.GetMasterVolume()

  rl.SetTargetFPS(120)

  target_time: f32 = f32(1000 / 4.0 / 1000)
  acc_time: f32 = 0.0

  skip_step := false
  reset := false
  just_started := true
  playing := false
  failed := true

  for !rl.WindowShouldClose() {
    free_all(context.temp_allocator)

    rl.UpdateMusicStream(music)

    acc_time += rl.GetFrameTime()

    if rl.IsKeyReleased(.SPACE) {
      if !playing {
        playing = true
        failed = false
        just_started = false
        acc_time = 0

        snake_length = 1
        slice.zero(snake[:])

        head_velocity = 0
        next_velocity = 0

        snake[0].x = 8
        snake[0].y = 8

        if !rl.IsMusicStreamPlaying(music) {
          rl.PlayMusicStream(music)
        }
      }
    }

    if rl.IsKeyDown(.W) {
      next_velocity = {0, -1}
    } else if rl.IsKeyDown(.S) {
      next_velocity = {0, 1}
    } else if rl.IsKeyDown(.A) {
      next_velocity = {-1, 0}
    } else if rl.IsKeyDown(.D) {
      next_velocity = {1, 0}
    }

    if rl.IsKeyReleased(.R) {
      reset = true
    }

    if acc_time >= target_time && skip_step {
      acc_time = 0
      skip_step = false
    } else if playing && acc_time >= target_time {
      acc_time = 0

      // move body
      clear_grid()
      for i := snake_length - 1; i > 0; i -= 1 {
        snake[i] = snake[i - 1]
        snake_cell(snake[i])
      }

      // disallow turning 180
      if head_velocity + next_velocity == 0 {
        next_velocity = 0
      }

      if next_velocity != 0 {
        head_velocity = next_velocity
      }
      next_velocity = 0

      snake[0] += head_velocity

      // handle window bounds crossing
      if snake[0].x < 0 {
        snake[0].x = CELLS_PER_ROW - 1
      } else if snake[0].x >= CELLS_PER_ROW {
        snake[0].x = 0
      }

      if snake[0].y < 0 {
        snake[0].y = CELLS_PER_ROW - 1
      } else if snake[0].y >= CELLS_PER_ROW {
        snake[0].y = 0
      }

      failed = is_snake_cell(snake[0])
      if reset || failed {
        playing = false
      }

      if failed {
        rl.StopMusicStream(music)
        rl.PlaySound(game_over_sound)
      }

      snake_cell(snake[0])

      if snake[0] == target {
        snake_length += 1
        target = generate_target(snake[0])

        clear_grid()
        for i := snake_length - 1; i > 0; i -= 1 {
          snake[i] = snake[i - 1]
          snake_cell(snake[i])
          skip_step = true
        }
      }
    }

    rl.BeginDrawing()
    rl.ClearBackground(rl.DARKGRAY)

    if just_started {
      text := cstring("Press SPACE to play")
      text_width := rl.MeasureText(text, 32)
      rl.DrawText(text, WIDTH / 2 - text_width / 2, HEIGHT / 2, 32, rl.ORANGE)
    } else {
      for i := 1; i < snake_length; i += 1 {
        rl.DrawRectangleRec(
          {f32(snake[i].x * CELL_SIZE), f32(snake[i].y * CELL_SIZE), CELL_SIZE, CELL_SIZE},
          rl.DARKGREEN,
        )
      }

      rl.DrawRectangleRec(
        {f32(snake[0].x * CELL_SIZE), f32(snake[0].y * CELL_SIZE), CELL_SIZE, CELL_SIZE},
        rl.GREEN,
      )

      radius: f32 = CELL_SIZE / 2.0
      rl.DrawCircle(
        i32(target.x * CELL_SIZE + int(radius)),
        i32(target.y * CELL_SIZE + int(radius)),
        radius,
        rl.RED,
      )

      status_bar(snake_length, failed, speaker_texture, &music_volume, music)
    }

    rl.EndDrawing()
  }
}

clear_grid :: proc() {
  slice.zero(Grid[:])
}

snake_cell :: proc(vec: Vec2) {
  i := vec.x + vec.y * CELLS_PER_ROW
  assert(i < TOTAL_CELLS, fmt.tprintf("%d %v", i, vec))
  Grid[i] = .Snake
}

is_snake_cell :: proc(vec: Vec2) -> bool {
  i := vec.x + vec.y * CELLS_PER_ROW
  assert(i < TOTAL_CELLS, fmt.tprintf("%d %v", i, vec))
  return Grid[i] == .Snake
}

status_bar :: proc(
  snake_length: int,
  failed: bool,
  speaker_texture: rl.Texture2D,
  music_volume: ^f32,
  music: rl.Music,
) {
  // status bar
  rl.DrawRectangleRec({0, HEIGHT, WIDTH, STATUS_BAR_HEIGHT}, rl.BLACK)

  points := fmt.tprintf("%d", snake_length - 1)
  rl.DrawText(cstring(raw_data(points)), 10, HEIGHT + 10, 32, rl.RED)

  if failed {
    game_over := cstring("Game Over")
    game_over_width := rl.MeasureText(game_over, 26)
    rl.DrawText(game_over, WIDTH / 2 - game_over_width / 2, HEIGHT + 2, 26, rl.RED)

    start_over := cstring("Press SPACE to start over")
    start_over_width := rl.MeasureText(start_over, 9)
    rl.DrawText(start_over, WIDTH / 2 - start_over_width / 2, HEIGHT + 2 + 26 + 2, 9, rl.RED)
  }

  sound_icon(speaker_texture, music_volume, music)
}

sound_icon :: proc(speaker_texture: rl.Texture2D, music_volume: ^f32, music: rl.Music) {
  sound_texture_source := rl.Rectangle {
    x      = 0,
    y      = 0,
    width  = f32(speaker_texture.width),
    height = f32(speaker_texture.height),
  }
  sound_texture_dest := rl.Rectangle {
    x      = WIDTH - 40,
    y      = HEIGHT + 6,
    width  = 32,
    height = 32,
  }

  rl.DrawTexturePro(speaker_texture, sound_texture_source, sound_texture_dest, {0, 0}, 0, rl.WHITE)

  mouse_position := rl.GetMousePosition()
  if rl.CheckCollisionPointRec(mouse_position, sound_texture_dest) {
    rl.DrawRectangleLinesEx(sound_texture_dest, 2.0, rl.WHITE)
    rl.SetMouseCursor(.POINTING_HAND)

    if rl.IsMouseButtonReleased(.LEFT) {
      music_volume^ = 1.0 - music_volume^
      rl.SetMusicVolume(music, music_volume^)
    }
  } else {
    rl.SetMouseCursor(.DEFAULT)
  }

  if music_volume^ <= 0.001 {
    rl.DrawLineEx(
      {sound_texture_dest.x, sound_texture_dest.y},
       {
        sound_texture_dest.x + sound_texture_dest.width,
        sound_texture_dest.y + sound_texture_dest.height,
      },
      3.0,
      rl.RED,
    )
  }
}
